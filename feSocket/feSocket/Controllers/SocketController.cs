﻿using System;
using System.Threading.Tasks;
using feSocket.Model;
using feSocket.Service;
using Microsoft.AspNetCore.Mvc;

namespace feSocket.Controllers
{
    [ApiController]
    public class SocketController : ControllerBase
    {
        private readonly MqttServices _mqttServices;
        public SocketController(MqttServices mqttServices)
        {
            _mqttServices = mqttServices;
        }

        [Route("/api/send")]
        [HttpPost]
        public async Task<IActionResult> Send([FromForm] SendRequest request)
        {
            if (string.IsNullOrEmpty(request.Topic))
                return Ok();

            await _mqttServices.Publish(request.Topic, request.Data);
            return Ok();
        }
    }
}
