﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;

namespace feSocket.Service
{
    public class MqttServices
    {
        private IMqttClient _mqttClient;
        private IMqttClientOptions _options;
        private MqttFactory _factory;
        public MqttServices(IMqttClientOptions options, MqttFactory mqttFactory)
        {
            _options = options;
            _factory = mqttFactory;
            _mqttClient = _factory.CreateMqttClient();
            Connect();
            Reconnect();
        }

        private void Connect()
        {
            _mqttClient = _factory.CreateMqttClient();
            _mqttClient.ConnectAsync(_options).Wait();
        }

        public async Task Publish(string topic, string payload)
        {
            try
            {
                var message = new MqttApplicationMessageBuilder()
                    .WithTopic(topic)
                    .WithPayload(payload)
                    .WithAtLeastOnceQoS()
                    .WithRetainFlag()
                    .Build();
                await _mqttClient.PublishAsync(message);
                Console.WriteLine("publish message ");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public void Reconnect()
        {
            _mqttClient.UseDisconnectedHandler(async e =>
            {
                Console.WriteLine("DISCONNECTED FROM SERVER");
                await Task.Delay(TimeSpan.FromSeconds(2));
                try
                {
                    if (_mqttClient.IsConnected == false)
                    {
                        await _mqttClient.ConnectAsync(_options, CancellationToken.None);
                    }
                }
                catch (Exception x)
                {
                    Console.WriteLine("RECONNECTING FAILED");
                    Console.WriteLine(x.Message);
                }
            });
        }
    }
}
