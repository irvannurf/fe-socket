﻿using System;
namespace feSocket.Model
{
    public class SendRequest
    {
        public string Topic { get; set; }
        public string Data { get; set; }
    }
}
